const app = require('../index').app;

const User = require('../models/User');

const chai = require('chai'),
    chaiHttp = require('chai-http');
chai.use(chaiHttp);

const exp = chai.expect;
const should = chai.should();

describe("Cards", () => {
it('http://localhost:5000/api/cards/ GET OK 200', (done) => {
    chai.request('http://localhost:5000/api/cards')
        .get('/')
        .end((err, res) => {
            if (err) console.error(err);
            exp(err).to.be.null;
            exp(res).to.have.status(200);
            res.body.getAll.should.all.have.property('name').eql('getAll');
            return done();
        });
});

it('http://localhost:5000/api/cards/:id GET OK 200', (done) => {
    chai.request('http://localhost:5000/api/cards')
        .get('/:3')
        .end((err, res) => {
            if (err) console.error(err);
            exp(err).to.be.null;
            exp(res).to.have.status(200);
            res.body.getById.should.all.have.property('name').eql('getById');
            return done();
        });
});

it('http://localhost:5000/api/cards/cards/add POST OK 200', (done) => {
    let add = {
        name: "addTest"
    }
    chai.request('http://localhost:5000/api/cards')
        .post('/add')
        .send(add)
        .end((err, res) => {
            if (err) console.error(err);
            exp(err).to.be.null;
            res.should.have.status(200);
            res.body.add.should.all.have.property('name').eql("addTest");
            return done();
        });
})

it('http://localhost:5000/api/cards/test/test GET FAIL 404', (done) => {
    chai.request('http://localhost:5000/api/cards/test')
        .get('/test')
        .end((err, res) => {
            if (err) console.error(err);
            exp(err).to.be.null;
            exp(res).to.have.status(404);
            return done();
        });
});
});


describe("Auth", () => {
it('http://localhost:5000/api/auth/login POST OK 200', (done) => {
    let login = {
        email: "login@login.com",
        password: "login666"
    }
    chai.request('http://localhost:5000/api/auth')
        .post('/login')
        .send(login)
        .end((err, res) => {
            if (err) console.error(err);
            exp(err).to.be.null;
            res.should.have.status(200);
            res.body.login.should.all.have
                        .property("email").eql("login@login.com");
            res.body.login.should.all.have
                        .property("password").eql("login666");
            return done();
        });
    });

    it("http://localhost:5000/api/auth/register POST OK 200", (done) => {
        let email = Math.floor(Math.random() * 10000);
        let register = new User({
            email: email,
            password: "register"
                });
        chai.request('http://localhost:5000/api/auth')
            .post('/register')
            .send(register.save())
            .end((err, res) => {
                    if (err) done(err);
                    else done();
                })
            })

    });

