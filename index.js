// Подключаем модуль app
const app = require('./app');
// Присваиваем порт из переменной окружения, если ничего так нету, 
// то присваиваем 5000 порт
const PORT = process.env.PORT || 5000;

// Начинаем прослушку
app.listen(PORT, () => console.log(`Server has been started on ${PORT} port`));

module.exports = app; // для теста