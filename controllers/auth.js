/* В этом контроллере делаю всю внутреннию логику авторизации */
const User = require('../models/User'); // Подключаю модель юзера
const bcrypt = require('bcrypt'); // Для генерации защищеного пароля
const keys = require('../config/keys');
module.exports.login = async function (req, res) {
    const candidate = await User.findOne({
                                email: req.body.email
                                });
    if (candidate) // делаем проверку, есть ли пользователь в базе
    {
        // Если пользователь существует
        // Проводим проверку пароля, введеного и зашифровонного в базе
        const passwordResult = bcrypt
                .compareSync(req.body.password, candidate.password);
        if (passwordResult) {
            res.status(200);
            req.session.candidate = candidate;
            return res.redirect('../users/'+candidate._id);
        }
        else
        {
            res.status(401).json({
                message: 'Пароль ввели не верно'
            })
        }
    }
    else
    {
        // Если нету
        res.status(404).json({
            message: 'Вас нету в базе, зарегистрируйтесь'
        })
    }
}

module.exports.register = async function(req, res) {
    // тут я произвожу поиск юзера в базе
    const candidate = await User.findOne({
                            email: req.body.email
                            });

    if (candidate)
        {
        // Если пользователь есть в базе, то сообщаем что уже есть
        res.status(409).json({
                    message: 'Такой email уже есть в базе'
                            });
        }
    else
        {
        // Если пользователя нет в базе, то создаем его
        const salt = bcrypt.genSaltSync(10); // Хэщ пароля
        const password = req.body.password;
        user = new User({
                    email: req.body.email,
                    // Генерируем пароль
                    password: bcrypt.hashSync(password, salt)
                });
        try
        {
            await user.save();
            res.status(201);
            return res.redirect('../auth/login/');
        }
        catch (error) // Обработчик ошибок
        {
            console.error(error);
        }
    }
}