const errorHandler = require('../utils/errorHandler');
const CardTitle = require('../models/CardTitle');
const CardField = require('../models/CardField');

module.exports.getAll = async function(req, res) {
    const userID = req.params.user;
    try
    {
        const titles = await CardTitle.find({
            user: userID
        });
        res.status(200).json(titles);
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.getById = async function (req, res) {
    try
    {
        // const user = req.session.candida
        // const card = await new CardTitle({
        //     title: req.body.title,
        // }).save();
        // res.status(201).json(card);

        let name = req.params.name;
        const card = await CardTitle.findOne({
            name: name
        })
        .catch(error => console.log('get id error', error));
        res.status(200).json(card);
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.add = async function(req, res) {
    
    try
    {   
        let cardTitle = await new CardTitle({
            // name: req.param('cardtitle'),
            // user: req.param('userid')
            name: req.body.cardTitle,
            user: req.body.userId
        })
        .save()
        .then(response => {
            console.log(response);
            res.status(201).json(response);
        })
        .catch(error => console.log('Backend add card title error', error));

    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.remove = async function(req, res) {
    try
    {
        await CardTitle.remove({_id: req.params.id});
        await CardField.remove({cardTitle: req.params.id});
        res.status(200).json('Карточка удалена');
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.update = async function(req, res)
{
    const idCard = req.params.id;
    const text = req.body.text;

    try
    {
        await CardTitle.findOneAndUpdate({
            _id: idCard
        },
        {
            $set: {name: text}
        }, (err, doc) => {
            if (err) console.log('Something wrong:', err);
            console.log(doc);
        })
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

// сохраняем позицию
// module.exports.position = async function(req, res)
// {
//     const idCard = req.params.id;
//     const top = req.body.top;
//     const left = req.body.left;

//     try
//     {
//         await CardTitle.findOneAndUpdate({
//             _id: idCard
//         },
//         {
//             $set: {
//                 positionTop: top,
//                 positionLeft: left}
//         }, (err, doc) => {
//             if (err) console.log('Something wrong:', err);
//             console.log(doc);
//         })
//     }
//     catch(error)
//     {
//         errorHandler(res, error);
//     }
// }

