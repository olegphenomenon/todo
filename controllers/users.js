const User = require('../models/User');
const CardTitle = require('../models/CardTitle');
const CardField = require('../models/CardField');

const errorHandler = require('../utils/errorHandler');

// Если успешно залогинился
module.exports.successLogin = async function (req, res, next) {
    const candidate = req.session.candidate;

    try {

        // Нахожу карточки пользователя
        let userData = await CardTitle.find({
            user: candidate._id
        }, err => {
            if (err) console.error(err);
        });

        /* Формирую объект карточки с наименованием и полями */
        const cardsLength = userData.length;
        let cards = [cardsLength];
        let listFields = [cardsLength];
        for (let i = 0; i < cardsLength; i++) {
            cards[i] = { title: userData[i].name,
                         lists: listFields[i] = await CardField.find({
                                cardTitle: userData[i]._id
                            })
            };
    }
            // ДЛЯ ПРИМЕРА И ОЗНАКОМЛЕНИЯ КАК МНЕ В БУДУЮЩЕМ РАЗРАБАТЫВАТЬ ЛОГИКУ
    //     // Выводим список полей каждой карточки пользователя, создаю объект
    //     // куда в поле title добавляю имя карточки, а в поле lists список
    //     // полей, которые относятся к данной карточке

    //     console.log(cards[4].title);
    //     console.log(cards[4].lists[0].fieldText)
    //     console.log(cards[4].lists.length)
    //     for(let i = 0; i < cards[4].lists.length; i++) 
    //         console.log(cards[4].lists[i].fieldText);
// ---------------------------------------------------------------------
        const sameData = 
        {
            pageTitle: 'Canvas',
            listFields: cards,
            candidate: req.session.candidate
        };
      return res.render('canvas', sameData);
    }

    catch (error) {
        errorHandler(error);
    }
};