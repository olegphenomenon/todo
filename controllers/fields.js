const errorHandler = require('../utils/errorHandler');
const CardField = require('../models/CardField');

module.exports.getAll = async function(req, res) 
{
    const cardID = req.params.link;
    console.log('card Id:', cardID);
    try
    {
        const titles = await CardField.find({
            cardTitle: cardID
        });
        return res.status(200).json(titles);
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.add = async function(req, res)
{
    try
    {
        const cardId = req.body.cardId;
        const cardText = req.body.fieldName;

        let field = new CardField({
            fieldText: cardText,
            cardTitle: cardId
        }).save().then( response => { 

            console.log(response);
            res.status(201).json(response);
        });

        console.log(field);
        // return res.status(201).json(field);
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}



module.exports.remove = async function(req, res)
{
    const idField = req.params.id;
    try
    {
        await CardField.remove({
            _id: idField
        });
        res.status(200).json({message: 'Поле удаленно'});
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}

module.exports.mark = async function(req, res) {
    const idField = req.params.id;
    const status = req.body.status;
    try {
        await CardField.findOneAndUpdate({
            _id: idField
        },
        {
            $set: {status: status}
        }, (err, doc) => {
            if (err) console.log('Something wrong:', err);
            console.log(doc);
        })
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}


module.exports.update = async function(req, res)
{
    const idField = req.params.id;
    const text = req.body.text;

    try
    {
        await CardField.findOneAndUpdate({
            _id: idField
        },
        {
            $set: {fieldText: text}
        }, (err, doc) => {
            if (err) console.log('Something wrong:', err);
            console.log(doc);
        })
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}


module.exports.getByTitle = async function(req, res)
{
    try
    {
        
    }
    catch(error)
    {
        errorHandler(res, error);
    }
}
