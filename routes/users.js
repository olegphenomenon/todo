const express = require('express');
const passport =require('passport');
const controllers = require('../controllers/users');

const router = express.Router();


router.get('/:slug', controllers.successLogin);

module.exports = router;