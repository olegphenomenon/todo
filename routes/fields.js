const express = require('express');

const controllers = require('../controllers/fields');
const router = express.Router();

router.get('/:link', controllers.getAll); 

// router.get('/:id');

router.post('/add', controllers.add);

router.delete('/:id', controllers.remove);

router.put('/mark/:id', controllers.mark); // помечаем выполнено или нет поле
router.put('/:id', controllers.update); // обновляем

// router.patch('/:id');

module.exports = router;
