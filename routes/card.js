const express = require('express');
const passport =require('passport');
const controllers = require('../controllers/card');
const router = express.Router();

// показывать все карточки
router.get('/:user', controllers.getAll); 
// выделить конкретную карточку
router.get('/id/:name', controllers.getById) 
// добавить карточку
router.post('/add', controllers.add); 
 // удалить карточку
router.delete('/:id', controllers.remove);
 // редактировать карточку
router.put('/:id', controllers.update);

// сохранить позицию
// router.put('/position/:id', controllers.position);

module.exports = router;