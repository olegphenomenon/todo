/* Модуль auth, где будут указаны роуты(маршруты) на регистрацию и вход */

const express = require('express');
const router = express.Router();

const controllers = require('../controllers/auth');


// router.get('/error', controllers.errorPage);

router.get('/login', function(req, res, next) { 
    res.render('login', {pageTitle: 'Login'});
});

router.get('/register', function(req, res, next) {
    res.render('register', {pageTitle: 'Register'});
});

router.post('/login', controllers.login);
router.post('/register', controllers.register);

module.exports = router;