const express = require('express');
const app = express();

// Сессии для передачи данных через redirect
const session = require('express-session');
app.use(session({secret: 'mySecret', resave: false, saveUninitialized: false}))

const path = require('path');

const keys = require('./config/keys');
// Подключаем базу
const mongoose = require('mongoose');
mongoose.connect(keys.mongoURI).then(() => console.log('MongoDB connected'))
                    .catch(error => console.log(error));


// BodyParser обрабатывает тела application/x-www-form-urlencoded и
// application/json запросов и выставляет для них req.body
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json()) // генерирует javascript объекты из получаемого json

app.use(require('morgan')('dev'));
app.use(require('cors')()); //https://developer.mozilla.org/ru/docs/Web/HTTP/CORS

//Шаблоны
app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Стили
app.use(express.static(path.join(__dirname, 'public')));

// роут для логина и регистрации
const authRouter = require('./routes/auth');
app.use('/api/auth', authRouter);

const cardRouter = require('./routes/card');
// роут для любого желающего по эксперементировать с карточками
app.use('/api/cards', cardRouter);

const fieldsRouter = require('./routes/fields');
app.use('/api/fields', fieldsRouter);

const userRouter = require('./routes/users');
app.use('/api/users', userRouter);


module.exports = app; // экспортируем объект express
