const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cardTitleSchema = new Schema({
    name: {
        type: String,
        default: 'test'
    },
    date: {
        type: Date,
        default: Date.now
    },
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    // координаты карточки
    // positionTop: {
    //     type: String
    // },
    // positionLeft: {
    //     type: String
    // }
});

module.exports = mongoose.model('cardTitles', cardTitleSchema);