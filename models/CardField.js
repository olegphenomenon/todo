const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const cardFieldSchema = new Schema({
    fieldText: {
        type: String
    },
    status: {
        type: Boolean,
        default: false
    },
    cardTitle: {
        ref: 'cardTitles',
        type: Schema.Types.ObjectId
    }
});


module.exports = mongoose.model('cardFields', cardFieldSchema);