                <% for (let j = 0; j < listFields[i].lists.length; j++) { %>
                    <% if (listFields[i].lists[j].status == false) { %>
                        <li style="padding-top: 2px" id="taskElement" name="taskElement"><%=listFields[i].lists[j].fieldText%></li>
                    <% } else { %>
                        <li style="padding-top: 2px" id="taskElement" name="taskElement" class="done"><%=listFields[i].lists[j].fieldText%></li>
                <% } %> 
                <% } %> 


function addTask(e) {
    let item = document.createElement('li'); // создаю элемент списка
    let remove = document.createElement('span'); // а здесь значек закрыть
    let parent = e.parentNode.childNodes[1]
    
    value = e.value;
    
    item.innerText = value; // вношу в элемент списка текст

    item.addEventListener('click', function() {
        markDoneOrNot(this);
    });

    remove.innerText = 'x'; 
    remove.classList.add('close'); // добавляю класс 
    
    item.addEventListener('click', function() {
        removeTask(this);
    });

    item.appendChild(remove); // добавляю элемент закрыть как ребенок в элемент списка

    parent.appendChild(item) // а сам элемент в список
}

