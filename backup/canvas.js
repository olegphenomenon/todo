

 window.onload = function(){ 
    let taskField = document.getElementsByName("task");
    let userID = window.location.pathname.split('/')[3];

    let request = new XMLHttpRequest();
    
    // Делаю AJAX запрос для заполнения заголовок карточек
    request.open('GET', '/api/cards/'+userID, false);
    request.send();
    let cards = JSON.parse(request.responseText);
    let cardsTitle = document.querySelectorAll('#titleCards');
    let tasksList = document.querySelectorAll('#tasksList');
    let hiddenId = document.querySelectorAll('#hiddenId');
    for(let i in cardsTitle) { 
        try {
            // Добавляю массив индефикаторов карточек
            if (cards[i]._id != undefined)
            // построение списка задач для каждой карточки
                buildTasksTree(tasksList[i], cards[i]._id); 
                generateHiddenCardId(cards[i]._id, hiddenId[i]);
            let text = cards[i].name;
            cardsTitle[i].textContent = text; 
        } catch(e) { console.error(e); }
    }

    // функция генерирует невидимый индефикатор к каждой карточке
    function generateHiddenCardId(id, field)
    {
        let idCard = document.createElement('span');
        idCard.textContent = id;
        idCard.classList.add('hidden');
        field.appendChild(idCard);
    }

    // построение дерева задач к каждой карточке
    // tasksList - это конкретная карточка, куда следует добавлять список задач
    // link - это индефикатор карточки, к которой относятся задачи
    async function buildTasksTree(tasksList, link) {
        let ul = document.createElement('ul');
        request.open('GET', '/api/fields/'+link, false);
        request.send();
        let fieldList = JSON.parse(request.responseText);

        for(let i in fieldList) {
            let li = document.createElement('li');
            li.textContent = fieldList[i].fieldText
            if (fieldList[i].status == true)
                li.classList.add('done');
            liDone(li);
            ul.appendChild(li);
        }

        tasksList.appendChild(ul);
    }

    // Навешиваю событие изменить значение "выполнено-в процессе"
    function liDone(elem) {
        elem.addEventListener('click', function() {
            markDoneOrNot(this);
        })
    }

    // Навешиваю обработчик на каждое поле taskField
    for (let i = 0; i < taskField.length; i++) {
        taskField[i].addEventListener('keyup', function(e){
            if (e.keyCode === 13) {
                addTask(this);
                this.value = '';
            }
        })
    }

 }

// Добавляю задачи
function addTask(e) {
    let request = new XMLHttpRequest();
    let cardtext = e.value;
    let cardid = e.parentNode.childNodes[3].textContent;
    let body = 'cardid='+cardid+'&cardtext='+cardtext;
    request.open('POST', '/api/fields/add', true);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    request.send(body);
    return updateDisplay();
}

function updateDisplay()
{

}

// пометить выполнено или нет
function markDoneOrNot(e) {
    const nameClass = 'done';
    if (e.classList.contains(nameClass)) e.classList.remove(nameClass)
    else e.classList.add(nameClass);
}

// function removeTask(e) {
//     let removeEl = e.parentNode;
//     console.log(removeEl);
//     console.log(e);
//     removeEl.removeChild(e);
// }

// function saveInDb(e) {
//     console.log(e.parentNode.childNodes[0].innerText);
//     let l = e.parentNode.querySelectorAll("#taskElement").length;
//     let el = e.parentNode.querySelectorAll("#taskElement");

//     for(let i = 0; i < l; i++) {
//         console.log(el[i]);
//     }
// }