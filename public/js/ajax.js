let userId = window.location.pathname.split('/')[3]; // id пользователя

// загружаю карточки с базы данных пользователя
// на верхнем уровне загружаю наименования карточек
$.ajax({
    url: '/api/cards/' + userId,
        success: function(result) {
            let cardLength = result.length;

            for(let i = 0; i < cardLength; i++) {

                cardName = result[i].name;
                cardId = result[i]._id;
                positionTop = result[i].positionTop;
                positionLeft = result[i].positionLeft;

                // начинаю строить карточку, тут я добавляю наименование карточки и добавляю
                // необходимые поля
                let copyTemp = $($('#templateCard').prop("content")).clone();
                copyTemp.find('h3').text(cardName).append(minus + plus + removeCard);
                copyTemp.find('.card').attr('id', cardId)
                // тут я беру ID карточки и делаю get запрос на то, чтобы вывести поля, которые 
                // относятся к этой карточке
                $.ajax({
                    url: '/api/fields/' + result[i]._id,
                    success: function(result) {
                        let fieldsLength = result.length;

                        for(let i = 0; i < fieldsLength; i++) {
                                copyTemp.find('ul').append('<li>' +  result[i].fieldText
                                    + removeList + '</li>');
                                if (result[i].status === true)
                                    copyTemp.find('ul').children().last().addClass('done');
                                copyTemp.find('ul').children().last().attr('id', result[i]._id);
                        }
                        $('#container').append(copyTemp.find('.wrapper').html());
                        $('#container').sortable({
                            connectWith: "#container",
                            cursor: "move",
                            // stop: function(event, ui){
                            //     let userPosition = ui.position;
                            //     $(this).animate(userPosition);
                            //     savePosition(ui, userPosition);
                            // }
                        });

                        let newCard = $('#container').children().last();
                        newCard.position({top: positionTop, left: positionLeft})
                        console.log(positionLeft, positionTop)

                        newCard.find('#sortable').sortable({
                            revert: true,
                            cursor: "move"
                        });
                        $('#ul, li').disableSelection();
                    }

                })
            }
        }
})

// // сохраняем позицию перетаскиваемого элемента
// function savePosition(id, ui) {
//     // console.log(id);
//     console.log(ui.css('left'), ui.css('top'));
//     // // console.log('ui position top position:', ui.position().top);
//     // console.log('ui top css:', ui.css('top'))
//     // let left = 

//     $.ajax({
//         url: '/api/cards/position/' + id,
//         type: 'PUT',
//         data: { top: ui.css('top'), left: ui.css('left') }
//     }, function(result) {
//         console.log(result)
//     })
// }

// создаю картчоку и посылаю post api запрос
// copyTemp - это экземпляр карточки которое создается, т.е сама разметка, в которую в аттрибут id
// я добавляю индефикатор карточки
function ajaxCreateCard(cardName, copyTemp) {
    $.post('/api/cards/add', {
        cardTitle: cardName,
        userId: userId
    },
    function(data, status) {
        copyTemp.find('.card').attr('id', data._id)
        $('#container').append(copyTemp.find('.wrapper').html());
        let newCard = $('#container').children().last();
        newCard.find('#sortable').sortable({
            revert: true,
            cursor: "move"
        });
        $('#ul, li').disableSelection();;
    })
}

// добавляю задачу, передаю cardId - индефикатор карточки, куда добавил задачу
//  taskName - наименование задачи и itemElement - экземпляр разметки, чтоьы
// в аттрибут id присвоить индефикатор созданного объекта
function addItemElement(cardId, taskName, itemElement) {
    $.post('/api/fields/add', {
        cardId: cardId,
        fieldName: taskName
    },
    function(data, status) {
        itemElement.attr('id', data._id);
    })
}


// удаляю карточку
function deleteCard(id) {
    $.ajax({
        url: '/api/cards/' + id,
        type: 'DELETE',
        success: function(result) {
            console.log(result);
        }
    })
}

// удаляю задачу
function deleteItemElement(id) {
    $.ajax({
        url: '/api/fields/' + id,
        type: 'DELETE',
        success: function(result) {
            console.log(result);
        }
    })
}

// задача выполнена или нет
function completedOrNot(element) {
    let id = element[0].id;
    let status = element.hasClass('done'); // если задача выполнена, 
    // то посылаю в свойство status экземпляру true

    $.ajax({
        url: '/api/fields/mark/' + id,
        type: 'PUT',
        data: { status: status }
    }, function(result) {
        console.log(result)
    })

    console.log(id, element.hasClass('done'));
}

// изменяю текст списка
function  ajaxChangeItemText(el) {
    let id = el[0].id;
    let text = el.text();

    $.ajax({
        url: '/api/fields/' + id,
        type: 'PUT',
        data: { text: text }
    }, function(result) {
        console.log(result)
    })
}

// изменяю заголовок карточки
function ajaxChangeHeaderCard(el) {
    let id = el.parent().parent().attr('id');
    let text = el.text();

    $.ajax({
        url: '/api/cards/' + id,
        type: 'PUT',
        data: { text: text }
    }, function(result) {
        console.log(result)
    })
}


