const removeList = '<span class="remove"><i class="fas fa-trash"></i></span>';
const removeCard = '<span class="removeCard remove"><i class="fas fa-trash"></i></span>';

const minus =' <span id="minus"><i class="far fa-minus-square"></i></span>'
const plus = '<span id="plus" class="none"><i class="far fa-plus-square"></i></span>'

changeItemText();
changeTitleText();


// создаю карточку
$('body').on('click', '#newCard', function(e) {
    let cardName = 'New Card';
    let copyTemp = $($('#templateCard').prop("content")).clone();
    copyTemp.find('h3').text(cardName).append(minus + plus + removeCard);
    ajaxCreateCard(cardName, copyTemp);
});


// добавляем в список дело
$('body').on('keypress', '.card .target', function(e) {
    if (e.which === 13) {
        let taskName = $(this).val();
        $(this).val('');
        $(this).closest('.card').find('ul').append('<li>' + taskName + removeList + '</li>');

        let itemElement = $(this).closest('.card').find('ul').children().last();
        let id = $(this).closest('.card');
        addItemElement(id[0].id, taskName, itemElement)
    }
});

// изменяю заголовок-------------------------------------------------
function changeTitleText() {
    $('body').on('dblclick', '.card .title h3', function(e) {
        $(this).find('span').remove();
        let titleText = $(this).text();
        $(this).css('display', 'none');
        $(this).closest('.card').find('.changeTitle').css('display', 'block').val(titleText).focus();
        $('body').off('dblclick', '.card .title h3');
    })
}

$('body').on('keypress', '.card .changeTitle', function(e) {
    if (e.which === 13) {
        let inputText = $(this).val();
        $(this).css('display', 'none');
        $(this).closest('.card').find('h3').css('display', 'block').text(inputText)
            .append(minus + plus + removeCard);
        ajaxChangeHeaderCard($(this).prev());
        changeTitleText();
    }
})
// -------------------------------------------------------------------

// изменяю текст списка ----------------------------------------------
function changeItemText() {
    $('body').on('dblclick', '.card ul li', function(e) {
        $(this).find('span').remove();
        let itemText = $(this).text();
        $(this).text('');
        $(this).append('<input type="text">');
        $(this).find('input').val(itemText).focus();
        $('body').off('dblclick', '.card ul li');
            
    })
}

$('body').on('keypress', '.card li input', function(e) {
    let parent = ($(this).parent());
    if (e.which === 13) {
        let inputText = $(this).val();
        $(this).parent().text(inputText).append(removeList);
        // ajaxChangeItemText($(this));
        $(this).remove();
        ajaxChangeItemText(parent);
        changeItemText();
    }
})

// // ==================================================================

// при клике на другую область во время ввода данных сохраить введенные значения
// элемент списка
$('body').on('focusout', '.card li input', function(e) {
    let parent = ($(this).parent());
    let txt = $(this).val();
    li = $(this).parent();
    li.text(txt).append(removeList);
    $(this).remove();
    ajaxChangeItemText(parent);
    changeItemText();
})
// заголовок карточки

$('body').on('focusout', '.card .changeTitle', function(e) {
    let inputText = $(this).val();
    $(this).css('display', 'none');
    $(this).closest('.card').find('h3').css('display', 'block').text(inputText)
            .append(minus + plus + removeCard);
    changeTitleText();
    ajaxChangeHeaderCard($(this).prev());

})
// -----------------------------------------------------------------------------



// --------------------------------------------------------------------

// помечаем выполнено иль нет
$('body').on('click', '.card ul li', function(e) {
    $(this).toggleClass('done');
    completedOrNot($(this));
})

// удаляем элемент списка
$('body').on('click', '.card li span', function(e) {
    $(this).parent().fadeOut(500, function() {
        $(this).remove();
    })
    let id = $(this).parent();
    deleteItemElement(id[0].id);
})

// удаляем карточку
$('body').on('click', '.card .removeCard', function(e) {
    $(this).closest('.card').fadeOut(500, function() { 
        $(this).remove();
    })
    // определяю индефикатор карточки
    let id = $(this).closest('.card');
    deleteCard(id[0].id);
})

// сворачиваем список
$('body').on('click', '.card #minus', function(e) {
    $(this).closest('.card').find('.listItems').fadeToggle();
    $(this).addClass('none');
    $(this).parent().find('#plus').removeClass('none');
})

$('body').on('click', '.card #plus', function(e) {
    $(this).closest('.card').find('.listItems').fadeToggle();
    $(this).addClass('none');
    $(this).parent().find('#minus').removeClass('none');
})
